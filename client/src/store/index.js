import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default function (/* { ssrContext } */) {
  return new Vuex.Store({
    state: {
      messages: [],
    },

    mutations: {
      setMessage (state, data) {
        state.messages.push(data)
      },
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })
}
