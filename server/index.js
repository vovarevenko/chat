const Koa = require('koa')
const app = new Koa
const server = require('http').createServer(app.callback())
const io = require('socket.io')(server)

const port = process.env.PORT || 3000

server.listen(port, () => {
  console.log('Server listening at port %d', port)
})

let nextUserId = 1
let usersCount = 0

io.on('connection', socket => {

  socket.userId = nextUserId

  nextUserId++
  usersCount++

  // console.log('user #' + socket.userId + ' connected')

  socket.emit('connected', { usersCount })

  socket.broadcast.emit('user connected', { usersCount })

  socket.on('disconnect', reason => {
    usersCount--

    // console.log('user #' + socket.userId + ' disconnected:', reason)

    socket.broadcast.emit('user disconnected', { usersCount })
  })

  socket.on('new message', data => {
    console.log('[USER-' + socket.userId + ']\t' + data.name + ': ' + data.text)

    socket.broadcast.emit('new message', data)
  })

})
